import numpy as np

usuarios = {'admin':'admin','Ana':'1234','Jorge':'password'}

print("Escriba su nombre de usuario y contraseña:")
user = input("Usuario: ")
password = input("Contraseña: ")

f = open("usuarios.txt","w")

if user not in usuarios.keys():
    print("El usuario ingresado no existe. ¿Desea crearlo?\n 1. Sí \n 2. No")
    aux = int(input())
    if aux == 1:
        usuarios[user] = password
else:
    if password == usuarios.get(user):
        print("Usuario y contraseña correctos")
    else:
        print("Contraseña incorrecta. Acceso denegado.")

for i in usuarios.keys():
    f.writelines(i+":"+usuarios[i]+'\n')
f.close()

if usuarios[user] == 'admin':
    f2 = open("usuarios.txt","r")
    m = np.array(f2.readlines())
    for i in range(m.shape[0]):
        print(m[i], end='')
    f2.close()